module Simple

start syntax Program = Expr;

syntax Expr
	= ID                  // variables
	| NUM                 // integers
	| left (Expr "*" Expr | Expr "/" Expr )       // multiplication
	> left Expr "+" Expr       // addition
	| ID "(" Expr ")"     // function call
	| "(" Expr ")"        // parentheses
	;
	
// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z_] !<< [a-zA-Z_]+ !>> [a-zA-Z_];

// numbers
lexical NUM = [0-9] !<< [0-9]+ !>> [0-9];


public str pretty(Program p) {
	if(Expr e := p)
		return pretty(e);
	else
		return "[unknown program: \"<e>\"]";
}

public default str pretty(Expr e) {
	return "[unknown expr: \"<e>\"]";
}


public str pretty((Expr)`<ID i>`) {
	return "<i>";
}

public str pretty((Expr)`<NUM n>`) {
	return "<n>";
}

public str pretty((Expr)`<Expr e1>+<Expr e2>`) {
	return "(<pretty(e1)>+<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>*<Expr e2>`) {
	return "(<pretty(e1)>*<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>/<Expr e2>`) {
	return "(<pretty(e1)>/<pretty(e2)>)";
}
public str pretty((Expr)`(<Expr e>)`) {
	return "(<pretty(e)>)";
}

public str pretty((Expr)`<ID f>(<Expr e>)`) {
	return "<f>(<pretty(e)>)";
}
