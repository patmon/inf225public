module Evaluator
import Simple;
import String;
import ParseTree;
import IO;

@doc{A scope maps strings to values.}
alias Scope = map[str, Value];

@doc{An environment is a list (stack) of scopes.}
alias Env = list[Scope];

data Value
	= Int(int intValue)
	| Fun(ID argName, Expr expr, Env env)
	;


@doc{Look up an identifier in the environment.}
public Value lookup(ID name, Env env) {
	for(sc <- env) {
		if(unparse(name) in sc)
			return sc[unparse(name)];
	}
	
	throw "Undefined variable \'<unparse(name)>\' at <name@\loc>";
}

@doc{Declare an identifier, giving it a value. It must not already have
been declared in the same scope.}
public Env declare(ID name, Value val, Env env) {
	sc = env[0]; // get the current scope
	
	if(unparse(name) in sc) {
		throw "Variable \'<unparse(name)>\' already declared at <name@\loc>";
	}
	else {
		sc[unparse(name)] = val;
		env[0] = sc;
		return env;
	}
}

@doc{Update the value of a variable in the environment}
public Env assign(ID name, Value val, Env env) {
	for(i <- index(env)) {
		sc = env[i];
		
		if(unparse(name) in sc) { // find the scope in which the variable is declared
			sc[unparse(name)] = val;
			env[i] = sc;
			return env;
		}
	}
	
	throw "Undefined variable \'<unparse(name)>\' at <name@\loc>";
}

@doc{Make a new scope in the environment}
public Env enterScope(Env env) {
	return [(), *env];
}

@doc{Discard the current scope from the environment}
public Env exitScope(Env env) {
	return env[1..];
}

@doc{Make a new environment}
public Env newEnv() {
	return [()];
}


@doc{For debugging.}
public void printEnv(Env env) {
	i = 0;
	for(sc <- env) {
		println("  Scope <i>:");
		i = i + 1;
		for(k <- sc) {	
			if(Fun(argName, expr, fenv) := sc[k])
				println("  <k>: Fun(<unparse(argName)>, <unparse(expr)>, <fenv>)");
			else
				println("  <k>: <sc[k]>");
		}
	}
}

@doc{Evaluate a function declaration. The declaration, and the (as yet
unevaluated) body is added to the environment, together with the environment
in which the function was defined (for lexical scoping).}
public Env define((Decl)`fun <ID f>(<ID arg>) = <Expr body>;`, Env env) {
	envNew = declare(f,  Fun(arg, body, env), env);
	println("Defining function <f>(<arg>) = <body>;");
	printEnv(env);
	return envNew;
}

@doc{Evaluate a program.}
public Value eval((Program)`<Decl* decls><Expr e>`) {
	try {
		Env env = newEnv();
		for(Decl d <- decls) {
			env = define(d, env);
		}
			
		return eval(e, env);
	}
	catch ex: {
		println(ex);
		return Int(0);
	}
}

@doc{The value of an integer literal is the value of integer itself.}
public Value eval((Expr)`<NUM i>`, Env env) {
	return Int(toInt(unparse(i)));
}

@doc{The value of a variable is the value it's bound to in the environment.}
public Value eval((Expr)`<ID x>`, Env env) {
	return lookup(x, env);
}

@doc{Function calls. Find the function definition first; evaluate the
argument, then evaluate the function body in the function's definition
environment, with the argument is bound to the value.}
public Value eval((Expr)`<ID f>(<Expr e>)`, Env env) {
	fun = lookup(f, env);	
	arg = eval(e, env);
	
	if(Fun(argName, body, envIn) := fun) {
		Env funEnv = envIn;
		funEnv = enterScope(funEnv);
		funEnv = declare(f, Fun(argName, body, funEnv), funEnv);
		funEnv = declare(argName, arg, funEnv);
		v = eval(body, funEnv);
		funEnv = exitScope(funEnv);  // no effect here, since funEnv is discarded anyway
		return v;
	}
	else {
		throw "Expression should be a function";
	}
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>*<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue * eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>+<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue + eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>-<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue - eval(e2, env).intValue);
}

@doc{Arithmetic. Evaluate the operands, then apply the operator.}
public Value eval((Expr)`<Expr e1>/<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue / eval(e2, env).intValue);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1> == <Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue == eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1>\<<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue < eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1>\><Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue > eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1>\<=<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue <= eval(e2, env).intValue ? 1 : 0);
}

@doc{Comparison. Evaluate the operands, compare, then yield 1 if
true or 0 if false.}
public Value eval((Expr)`<Expr e1>\>=<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue >= eval(e2, env).intValue ? 1 : 0);
}

@doc{'If' expression. Evaluate the condition first, then evaluate and
return either of the branches depending on the result.}
public Value eval((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env env) {
	if(eval(c, env).intValue != 0)
		return eval(e1, env);
	else
		return eval(e2, env);
}


@doc{'Let' expression. Evaluate the first expression, then evaluate the
second in an environment where the variable is bound to the result of the
first expression.}
public Value eval((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env env) {
	v = eval(e1, env);
	
	innerEnv = enterScope(env);
	innerEnv = declare(x,  v, innerEnv);
	
	v2 = eval(e2, innerEnv);
	
	env = exitScope(innerEnv); // no effect here, since funEnv is discarded anyway
	
	return v2;
}

