module Evaluator
import Simple;
import String;
import ParseTree;
import IO;

data Value
	= Int(int intValue)
	| Fun(str argName, Expr expr)
	;

alias Env = map[str, Value];

public Env define((Decl)`fun <ID f>(<ID arg>) = <Expr body>;`, Env env) {
	env[unparse(f)] = Fun(unparse(arg), body);
	return env;
}

public Value eval((Program)`<Decl* decls><Expr e>`) {
	Env env = ();
	for(Decl d <- decls) {
		env = define(d, env);
	}
	
	for(k <- env) {
		println("<k>: <env[k]>");
	}
	
	return eval(e, env);
}

public Value eval((Expr)`<NUM i>`, Env env) {
	return Int(toInt(unparse(i)));
}

public Value eval((Expr)`<ID x>`, Env env) {
	return env[unparse(x)];
}

public Value eval((Expr)`<ID f>(<Expr e>)`, Env env) {
	fun = env[unparse(f)];	
	arg = eval(e, env);
	
	if(Fun(argName, body) := fun) {
		Env funEnv = ();
		funEnv[argName] = arg;
		return eval(body, funEnv);
	}
	else {
		throw "Expression should be a function";
	}
}

public Value eval((Expr)`<Expr e1>*<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue * eval(e2, env).intValue);
}

public Value eval((Expr)`<Expr e1>+<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue + eval(e2, env).intValue);
}

public Value eval((Expr)`<Expr e1>-<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue - eval(e2, env).intValue);
}

public Value eval((Expr)`<Expr e1>/<Expr e2>`, Env env) {
	return Int(eval(e1, env).intValue / eval(e2, env).intValue);
}

public Value eval((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`, Env env) {
	if(eval(c, env).intValue != 0)
		return eval(e1, env);
	else
		return eval(e2, env);
}


public Value eval((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`, Env env) {
	v = eval(e1, env);
	
	env[unparse(x)] = v;
	
	return eval(e2, env);
}

