module Pretty
import Simple;

public str pretty(Program p) {
	str result = "";
	for(Decl d <- p.decls)
		result = "<result><pretty(d)>\n";
	result += pretty(p.expr);
}

public str pretty((Decl)`fun <ID f>(<ID param>) = <Expr body>;`) {
	return "fun <f>(<param>) = <pretty(body)>;";
}

public default str pretty(Expr e) {
	return "[unknown expr: \"<e>\"]";
}

public str pretty((Expr)`<ID i>`) {
	return "<i>";
}

public str pretty((Expr)`<NUM n>`) {
	return "<n>";
}

public str pretty((Expr)`<Expr e1>+<Expr e2>`) {
	return "(<pretty(e1)>+<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>-<Expr e2>`) {
	return "(<pretty(e1)>+<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>*<Expr e2>`) {
	return "(<pretty(e1)>*<pretty(e2)>)";
}

public str pretty((Expr)`<Expr e1>/<Expr e2>`) {
	return "(<pretty(e1)>/<pretty(e2)>)";
}
public str pretty((Expr)`(<Expr e>)`) {
	return "(<pretty(e)>)";
}

public str pretty((Expr)`<ID f>(<Expr e>)`) {
	return "<f>(<pretty(e)>)";
}

public str pretty((Expr)`if <Expr c> then <Expr e1> else <Expr e2> end`) {
	return "if <pretty(c)> then <pretty(e1)> else <pretty(e2)> end";
}

public str pretty((Expr)`let <ID x> = <Expr e1> in <Expr e2> end`) {
	return "let <x> = <pretty(e1)> in <pretty(e2)> end";
}
