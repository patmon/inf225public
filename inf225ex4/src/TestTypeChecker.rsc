module TestTypeChecker
import Simple;
import Value;
import TypeChecker;
import ParseTree;
import Environment;
import IO;

test bool Numbers() { return getType(check((Expr)`5`, newEnv(#Type))) == Int(); }

test bool Arith1() { return getType(check((Expr)`5+5`, newEnv(#Type))) == Int(); }
test bool Arith2() { return getType(check((Expr)`1+2*3`, newEnv(#Type))) == Int(); }
test bool Arith3() { return getType(check((Expr)`10-2`, newEnv(#Type))) == Int(); }
test bool Arith4() { return getType(check((Expr)`10/2`, newEnv(#Type))) == Int(); }
test bool Arith5() { return getType(check((Expr)`(1+2)*3`, newEnv(#Type))) == Int(); }
test bool Arith6() { return getType(check((Expr)`((10)/(2))`, newEnv(#Type))) == Int(); }



test bool If1() {
	check((Program)`if 2*3 == 6 then 42 else 69 end`);
	return true;
}
test bool If2() {
	check((Program)`if 6 == 5 then 42 else 69 end`);
	return true;
}
test bool If3() {
	check((Program)`if 10 \< 5 then 1 else 0 end`);
	return true;
}

test bool IfFail1() {
	try {
		// condition should be int, not a function
		check((Program)`fun f() : int = 2; if f then 1 else 2 end`);
	}
	catch str _: return true;
	return false;
}
test bool IfFail2() {
	try {
		// branch types must be the same
		check((Program)`fun f() : int = 2; if 1 then 1 else f end`);
	}
	catch str _: return true;
	return false;
}

test bool Let1() {
	check((Program)`let x = 2 in x end`);
	return true;
}
test bool Let2() {
	check((Program)`fun f(int x) : int = x; let x = 2 in f(x) + x end`);
	return true;
}
test bool Let3() {
	check((Program)`fun f(int x) : int = x; let x = f in let x = 2 in x + 2 end end`);
	return true;
}

test bool LetFail1() {
	try {
		// undeclared variable
		check((Program)`let x = x in x end`);
	}
	catch str _: return true;
	return false;
}
test bool LetFail2() {
	try {
		// use variable after scope exit
		check((Program)`let x = 2 in x end; x`);
	}
	catch str _: return true;
	return false;
}

test bool AssignFail1() {
	try {
		// wrong type in assignment
		check((Program)`fun f(int x) : int = x; let x = f in x = 2 end`);
	}
	catch str _: return true;
	return false;
}

test bool Fun1() {
	return getType(check((Program)`fun f(int x) : int = 5; f`)) 
			== Fun([Int()], Int()); }
test bool Fun2() {
	return getType(check((Program)`fun f(int x) : int = 5; fun g() : int -\> int = f; g`)) 
			== Fun([], Fun([Int()], Int())); }
test bool FailFun1() {
	try {
		// wrong return type
		check((Program)`fun f(int x) : int = 5; fun g() : int -\> int = 5; 0`); 
	}
	catch str _: return true;
	return false;
}
test bool FailFun2() {
	try {
		// wrong return type
		check((Program)`fun f(int x) : int = 5; fun g() : int = f; 0`); 
	}
	catch str _: return true;
	return false;
}
test bool FailFun3() {
	try {
		// type error in body
		check((Program)`fun f(int -\> int x) : int = x + 5; 0`); 
	}
	catch str _: return true;
	return false;
}
