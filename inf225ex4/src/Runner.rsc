module Runner
import TypeChecker;
import Evaluator;
import Value;
import Simple;
import IO;

public Value run(Program p) {
	<p, typ> = check(p);

	<val, store> = eval(p);
	return val;
}
