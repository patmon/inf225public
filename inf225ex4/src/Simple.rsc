module Simple
import List;

start syntax Program = Decl* decls Expr expr;

syntax Decl
	= "fun" ID name "(" {ParamDecl ","}* paramList ")" ":" TypeExpr retType "=" Expr body ";"
	;
	 
syntax ParamDecl
	= TypeExpr paramType ID paramName
	;
	
syntax TypeExpr
	= SimpleTypeExpr
	// hand-code right-associativity of ->, since Rascal doesn't handle it automatically
	// when there's a list on the left-hand side
	| {SimpleTypeExpr ","}* "-\>" TypeExpr 
	;

syntax SimpleTypeExpr = "int" | "str" | "(" TypeExpr ")"
	;
	
syntax Expr
	= ID                  // variables
	| NUM                 // integers
	| ID "(" ArgList argList ")"     // function call
	| "(" Expr ")"        // parentheses
	> left (Expr "*" Expr | Expr "/" Expr )       // multiplication
	> left (Expr "+" Expr | Expr "-" Expr )       // addition
	> left (Expr "\>" Expr | Expr "\<" Expr | Expr "\>=" Expr | Expr "\<=" Expr | Expr "==" Expr )       // addition
	> right ID var "=" Expr e
	> left Expr ";" Expr
	| "let" ID "=" Expr "in" Expr "end" 
	| "if" Expr "then" Expr "else" Expr "end"
	;
	
syntax ArgList = {Expr ","}*;
// identifiers
//    y !<< x means 'x' must not be preceeded by  'y'
//    x !>> y means 'x' must not by followed by 'y'
// so, this means that an identifier is a sequence of one
// or more letters or underscores, with no additional
// letters or underscores before or after
lexical ID = [a-zA-Z_] !<< [a-zA-Z_] [a-zA-Z_0-9]* !>> [a-zA-Z_0-9];

// numbers
lexical NUM = [0-9] !<< [0-9]+ !>> [0-9];

layout LAYOUT = (SPC | COM)* !>> [\ \t\n\r\f] !>> "//";

lexical COM
	= @category="Comment" "//" ![\n\r]* $
	;

lexical SPC = [\ \t\n\r\f];

public ArgList makeArgList(list[Expr] as) {
	return parse(#ArgList, intercalate(", ", as));
} 
