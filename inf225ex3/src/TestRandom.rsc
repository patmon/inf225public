module TestRandom
import Simple;
import EvaluatorDynamic;
import ParseTree;

/* Random tests -- Rascal will automatically fill in random integers.
   To make these work, you also need to deal with negative numbers
   in the grammar:
     lexical NUM = [\-]? [0-9] !<< [0-9]+ !>> [0-9];
*/
test bool ArithRanAdd(int x, int y) {
	xTree = parse(#NUM, "<x>");
	yTree = parse(#NUM, "<y>");
	return eval((Program)`<NUM xTree>+<NUM yTree>`) == Int(x+y);
}
test bool ArithRanSub(int x, int y) {
	xTree = parse(#NUM, "<x>");
	yTree = parse(#NUM, "<y>");
	return eval((Program)`<NUM xTree>-<NUM yTree>`) == Int(x-y);
}
test bool ArithRanMul(int x, int y) {
	xTree = parse(#NUM, "<x>");
	yTree = parse(#NUM, "<y>");
	return eval((Program)`<NUM xTree>*<NUM yTree>`) == Int(x*y);
}
test bool ArithRanDiv(int x, int y) {
	if(y == 0)
		y = 1;
	xTree = parse(#NUM, "<x>");
	yTree = parse(#NUM, "<y>");
	return eval((Program)`<NUM xTree>/<NUM yTree>`) == Int(x/y);
}
test bool ArithRanComp(int x, int y) {
	xTree = parse(#NUM, "<x>");
	yTree = parse(#NUM, "<y>");
	return eval((Program)`<NUM xTree>==<NUM yTree>`) == Int(x==y ? 1 : 0)
	  &&	eval((Program)`<NUM xTree>\<<NUM yTree>`) == Int(x<y ? 1 : 0)
	  &&	eval((Program)`<NUM xTree>\><NUM yTree>`) == Int(x>y ? 1 : 0)
	  &&	eval((Program)`<NUM xTree>\<=<NUM yTree>`) == Int(x<=y ? 1 : 0)
	  &&	eval((Program)`<NUM xTree>\>=<NUM yTree>`) == Int(x>=y ? 1 : 0);
}
