module TestAssign
import Simple;
import EvaluatorDynamic;
import ParseTree;

test bool Numbers() { return eval((Program)`5`) == Int(5); }

test bool Arith1() { return eval((Program)`5+5`) == Int(10); }
test bool Arith2() { return eval((Program)`1+2*3`) == Int(7); }
test bool Arith3() { return eval((Program)`10-2`) == Int(8); }
test bool Arith4() { return eval((Program)`10/2`) == Int(5); }
test bool Arith5() { return eval((Program)`(1+2)*3`) == Int(9); }
test bool Arith6() { return eval((Program)`((10)/(2))`) == Int(5); }


test bool If1() { return eval((Program)`if 2*3 == 6 then 42 else 69 end`) == Int(42); }
test bool If2() { return eval((Program)`if 6 == 5 then 42 else 69 end`) == Int(69); }
test bool If3() { return eval((Program)`if 10 \< 5 then 1 else 0 end`) == Int(0); }

test bool OperatorPriority01() {
	return eval((Program)`let x = 5 in x = 2; 3 end`) 
	    == eval((Program)`let x = 5 in (x = 2); 3 end`);
}
				
test bool OperatorPriority02() {
	return eval((Program)`let x = 5 in let y = 4 in x = y = 2 end; x end`) 
	    == eval((Program)`let x = 5 in let y = 4 in x = (y = 2) end; x end`) ;
}

test bool OperatorPriority03() {
	return eval((Program)`let x = 5 in let y = 4 in x = y = 2; y end end`) 
	    == eval((Program)`let x = 5 in let y = 4 in (x = (y = 2)); y end end`) ;
}

test bool OperatorPriority04() {
	return eval((Program)`let x = 5 in let y = 4 in x = 1; y = 2; x = y; y = x + 1; x end end`) 
	    == eval((Program)`let x = 5 in let y = 4 in ((((((x = 1)); (y = 2)); (x = y)); (y = x + 1)); x) end end`) ;
}

test bool Shadowing1() {
	return eval((Program)`let x = 5 in let x = 4 in x = 1 end; x end`) 
	    == Int(5);
}

test bool Shadowing2() {
	return eval((Program)`let x = 5 in let y = 4 in x = 1 end; x end`) 
	    == Int(1);
}

test bool Shadowing3() {
	return eval((Program)`let z = 0 in let x = 5 in let x = 4 in let y = 2 in x = 1 end; if x == 1 then x = 3 else x = 4 end; z = x end end; z end`) 
	    == Int(3);
}
